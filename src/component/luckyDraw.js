import { Component } from "react";

class LuckyDraw extends Component {
    clickHandler = () => {
        console.log("abc");
        this.props.generate();
    }

    render() {
        return (
            <div className="container">
                <div className="row mt-5">
                    <div className="col-12 text-center">
                        <h3>Lucky Draw</h3>
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col-2">
                        <div className="circle">{this.props.ball1}</div>
                    </div>
                    <div className="col-2">
                        <div className="circle">{this.props.ball2}</div>
                    </div>
                    <div className="col-2">
                        <div className="circle">{this.props.ball3}</div>
                    </div>
                    <div className="col-2">
                        <div className="circle">{this.props.ball4}</div>
                    </div>
                    <div className="col-2">
                        <div className="circle">{this.props.ball5}</div>
                    </div>
                    <div className="col-2">
                        <div className="circle">{this.props.ball6}</div>
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col-12 text-center">
                        <button className="btn btn-success" onClick={this.clickHandler}>Generate</button>
                    </div>
                </div>

            </div>
        )
    }
}
export default LuckyDraw;